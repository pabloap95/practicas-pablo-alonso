<?php
require_once 'pagination.php';
require ('../Utils/conexion.php');

$pagination = new pagination();

$_page = (int) $_GET["page"];
if ($_page > 0) {
    $_page--;
} else {
    $_page = 0;
}
$_max_item = 10;
$_offset = $_max_item * $_page;

$query = "select * from facturas limit $_max_item offset $_offset";
$_str_query = "select * from facturas";

$result = mysqli_query($con, $query);
?>
<div class="container-fluid">
    <h1>Lista de facturas</h1>
    <table class="table table-sm table-bordered table-hover">
        <tr>
            <th>Index</th>
            <th>Número Factura</th>
            <th>Fecha</th>
            <th>NIF</th>
            <th>EurosBI</th>
            <th>IVA</th>
            <th>EurosIVA</th>
            <th>Total</th>
        </tr>
        <?php
        $index = 0;
        while ($row = mysqli_fetch_array($result)) {
            $index++;
            ?>
            <tr>
                <td><?php echo ($index + $_offset); ?></td>
                <td><?php echo $row["numFactura"]; ?></td>
                <td><?php echo $row["fecha"]; ?></td>
                <td><?php echo $row["dni"]; ?></td>
                <td><?php echo $row["eurosBI"]; ?></td>
                <td><?php echo ($row["iva"]*100)."%" ?></td>
                <td><?php echo $row["eurosIVA"]; ?></td>
                <td><?php echo $row["eurostotal"]; ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <hr>

    <?php
    $_js_function_name = "todasFacturas";
    $pagination->btn_primary($_str_query, $_page, $_max_item, $_js_function_name, $con);
    ?>
</div>