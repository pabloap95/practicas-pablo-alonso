<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Catalogo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>  

</head>
<body>
  <div class="container-fluid">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php">Catálogo</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     <li><a class="nav-item nav-link active" href="#" onclick="mostrarCatalogo()">Comprar<span class="sr-only">(current)</span></a></li>
     <li> <a class="nav-item nav-link active" href="#" onclick="misdevol()">Devoluciones</a></li>
      <li><a class="nav-item nav-link active" href="#" onclick="almacen()">Listado Artículos</a></li>
      <li><a class="nav-item nav-link active" href="#" onclick="todasFacturas()">Listado Ventas</a></li>
    </ul>
  </div>
</nav>
</div>
<div class="container-fluid" id="catalbody" style="display: block;">
	<h1 class="text-center">Bienvenido a tu tienda online!</h1>
    <img src="Imagenes/tiendalogo.JPG" alt="tiendalogos" class="img-fluid img-thumbnail">	
</div>
<div class="container-fluid">
<div id="listaproductos" style="overflow-x:auto;">
<h1 class="text-center" id="titproductos" style="display: none;">Nuestros productos:</h1>
<div id="productos"></div>
<div>
	<h1 id="titulolineas" class="container-fluid"></h1>
	<table id="lineas" style="text-align: center; margin-left:1%;"></table>
	<h2 id="final" class="container-fluid"></h2>
	<p id="comprafinal" class="container-fluid"></p>
</div>
</div>
<div id="tunif"></div>
</div>
<div class="container-fluid">
		<p id="mensajenoregistrado" style="color: red;"></p>
	    <div id="datos" style="display: none;">
	    	<form name='registroForm' id="registroForm" method="post" enctype="multipart/form-data">
	    		<table>
	    			<tr><td>NIF: </td><td><input type='text' value='' id='reg_NIF' name='reg_NIF'onblur="validarNIF()"></td><td><p style="color: red;" id="errordni"></p></td></tr>
	    			<tr><td>Nombre: </td><td><input type='text' value='' name='reg_nombre' id='reg_nombre' onblur="validarnombre()"></td><td><p style="color: red;" id="errornombre"></p></td></tr>
	    			<tr><td>Apellidos: </td><td><input type='text' value='' name='reg_apellidos' id='reg_apellidos' onblur="validarapellidos()"></td><td><p style="color: red;" id="errorapell"></p></td></tr>
	    			<tr><td>Dirección: </td><td><input type='text' value='' name='reg_dir' id='reg_dir' placeholder="Calle Tu Direccion" onblur="validardirec()"></td><td><p style="color: red;" id="errordir"></p></td></tr>
	    			<tr><td>Población: </td><td><input type='text' value='' name='reg_poblacion' id='reg_poblacion' onblur="validarpoblacion()"></td><td><p style="color: red;" id="errorpob"></p></td></tr>
	    			<tr><td>Correo: </td><td><input type='email' value='' name='reg_correo' id='reg_correo' placeholder="tucorreo@mail.com" onblur="validarcorreo()"></td><td><p style="color: red;" id="errorcor"></p></td></tr>
	    		</table>
	    			<button type='button' id='mybtn' class='btn btn-primary' style="margin-top: 1%;"  onclick='misvalidaciones()'>Confirmar</button>
	    		</form>
	    </div>
	    <button type='button' id='pagobtn' class="btn btn-success" onclick='pagar()' style="display: none;margin-top: 2%;">Pagar</button>
	    
</div>
</div>

<div id="devol" style="display: none;" class="container-fluid">
	<h1>Bienvenido al sistema de devoluciones</h1>
	<table>
		<tr>
			<td>Introduce tu NIF: </td>
			<td><input type="text" id="nifdevolucion" onblur="nifdevolvalidacion()"></td><td><p style="color: red;" id="errornifdevol"></p></td>
		</tr>
		<tr>
			<td>Introduce tu número de factura: </td>
		    <td><input type="text" id="numfacturadevol" onblur="numdevolvalidacion()"></td><td><p style="color: red;" id="errornumdevol"></p></td>	
		</tr>
	</table>
	<button type="button" id="btndevol" class="btn btn-success" onclick="devolvalidaciones()">Continuar</button>
	<div>
		<p style="color: red;" id="message"></p>
		<table id='tabladevoluciones' style="display: none;">
		</table>
	</div>
	<div>
		<h1 id="titulodevol" style="display: none;">Tus devoluciones: </h1>
			<table id="listaadevolver" style="display: none;"></table>
			<button type="button" id="btnform" class="btn btn-success" style="display: none; margin-top: 1%;" onclick="enviardatos()">Enviar</button>
	</div>
</div>
<div class="container-fluid">
	<h1 id="tituloarts" style="display: none;">Lista de artículos</h1>
	<table id="tablaalmacen" class="table table-sm table-bordered table-hover" style="display: none;"></table>
</div>
<div style="display: none;" id="tablafacturas">
</div>
  
  <div class="container-fluid">
  <div class="main-row jumbotron p-2" style="margin-top:1%;">
  	<div class="row">
    <div class="col-md-6">
      <p class="d-flex justify-content-center justify-content-md-start">Tienda Online</p> 
    </div>
      <div class="col-md-6"> 
  <ul class="list-inline d-flex justify-content-center justify-content-md-end">
    <li class="list-inline-item"><a href="index.php">Inicio</a> </li>
    <li class="list-inline-item"><a href="#">Contacto</a> </li>
  </ul>
  </div> 
  </div>
  </div>
  </div> 

<script>
	//Arrays para pasarle las cantidades de la base de datos y trabajar con ellos aquí
 	var cantidades = new Array(); //Cantidad que se irán acumulando según se compre el producto
 	var precios = new Array(); //PVP
 	var maxCant = new Array(); //Cantidad inicial que irá disminuyendo
 	var minCant = new Array(); //Cantidad Mínima
 	var precioTotal = new Array(); //Array para ir sumando posteriormente los precios de los artículos comprados
    var cantidadIni = new Array(); //Cantidad Inicial

 	//Función para mostrar el catálogo con los productos a comprar
	function mostrarCatalogo(){
	devol.style.display = "none";
	listaproductos.style.display = "block";
	tablaalmacen.style.display = "none";
	tablafacturas.style.display = "none";
	catalbody.style.display = "none";
    tituloarts.style.display="none";

	var objAjax = new XMLHttpRequest(); 
			objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1)
							alert(objAjax.responseText);
						
						else { 
							titproductos.style.display= "block";
							var listado = "<table id='tablaproductos' class='table table-sm table-bordered table-hover'>";
							var tienda = new Array(); //A través de Ajax obtengo la tabla de los artículos
							tienda = JSON.parse(this.responseText); //Transformación a un array de JS
							for (i=0; i< tienda.length; i++){
								cantidades.push(0); //Inicialización de cantidades y precios a 0
								precioTotal.push(0);
								maxCant.push(tienda[i].cantidad);
								precios.push(tienda[i].pvp);
								minCant.push(tienda[i].cantidadMin)
								cantidadIni.push(tienda[i].cantidad);
								listado += "<tr text-align:center;'><td><img style='width:240px;height:240px;' src='"+ tienda[i].imagenes +"'></td><td>"+ tienda[i].nombre+ "</td><td>" + tienda[i].pvp +"</td><td><button type='button' class='btn btn-info' id='product"+i+"'onclick='mostrarLineas(this)'>Añadir al carrito</button></td></tr>";

							}

							listado += "</table>";
							productos.innerHTML = listado;
						}
				};
				objAjax.open("GET", "Menu/listado.php", true);
				objAjax.send(null);
				 }
				 //Objeto para construir las lineas
			class Linea{
				constructor(codArticulo,fecha,cantidad,preciopvp,dni,sumaTotal){
					this.codArticulo = codArticulo;
					this.fecha = fecha;
					this.cantidad = cantidad;
					this.preciopvp = preciopvp;
					this.dni= dni;
					this.sumaTotal = sumaTotal;
				}
			}
			//Array para guardar todas las líneas
			var TodasLineas = new Array();
			var suma = 0;
			//Según se clique el botón para comprar el producto irán apareciendo los productos
    function mostrarLineas(a){
    	devol.style.display = "none";
        tablaalmacen.style.display = "none";
        tablafacturas.style.display = "none";

	            var id = a.id;
	            var objAjax = new XMLHttpRequest();
			objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1)
							alert(objAjax.responseText);
						
						else { 					  
							suma=0;
							var tienda = new Array();
							tienda = JSON.parse(this.responseText);
							for (i=0; i< tienda.length; i++){
								if (id == "product"+i){
								document.getElementById("product"+i).style.display = "none";
								maxCant[i]--;
								if (cantidades[i] <= tienda[i].cantidad){
                                cantidades[i]++;
								var actualizacion = "<tr id='trid"+i+"'><td><img style='width:60px;height:60px;' src='"+ tienda[i].imagenes +"'></td><td>"+ tienda[i].nombre+ "</td><td>" + tienda[i].pvp +"€</td><td><input type='text' id='inp"+i+"' value='"+cantidades[i]+"' size='1'></td><td><button type='button' id='menos"+i+"' onclick='menoscantidad(this)'>-</button></td><td><button type='button' id='mas"+i+"' onclick='mascantidad(this)'>+</button</td></tr>";
								var hoy = new Date(); //Cálculo de la fecha a día de hoy
								var formato = hoy.getFullYear() + "-" + (hoy.getMonth() +1) + "-" + hoy.getDate();
							    //Si la cantidad del producto es solo 1
                                if(cantidades[i]==1){
                                precioTotal[i]=(precios[i]*(cantidades[i]));}
                                //Añadimos cada línea al array de todas las líneas
                                var cadaLinea = new Linea(tienda[i].codArticulo,formato,cantidades[i],tienda[i].pvp,"dni","suma");
                                TodasLineas.push(cadaLinea);
								} else{
									alert("No tenemos cantidad suficiente");
								}
								//Comprobación de si hay stock suficiente. De no haberlo se le envía un correo al 
						        //proveedor y se le añade cantidad

                                if(tienda[i].cantidadMin >= maxCant[i]){
                                	if (tienda[i].pvp > 100.00){
                                		alert("Límite de stock alcanzado. En caso de pago final se enviará un correo al proveedor para soliciatar nuevas unidades.");
                                		maxCant[i] +=5;
                                		console.log(tienda[i].cantidad);
                                		console.log(maxCant[i]);
                                	} else{
                                		alert("Límite de stock alcanzado. En caso de pago final se enviará un correo al proveedor para soliciatar nuevas unidades.");
                                		maxCant[i] +=15;
                                }
                                }
							}
							}
							console.log(maxCant);
							console.log(precios);
							//En la suma se añaden todos los precios de los productos comprados
                            for (x=0; x<precioTotal.length;x++){
                            	suma += parseFloat(precioTotal[x]);


                            }
							
							titulolineas.innerHTML = "<h1>Tu carrito:</h1>";
							
							lineas.innerHTML += actualizacion;

							final.innerHTML = "Total: " + suma + "€";

							comprafinal.innerHTML = "<button type='button' class='btn btn-primary' id='btncompra' onclick='comptuNIF()'>Comprar</button>";

							console.log(TodasLineas);
							
						}
				};

				objAjax.open("GET", "Menu/listado.php", true);
				objAjax.send(null);
	            }
                
                //Función para aumentar la cantidad comprada
	            function mascantidad(b){
                var i = b.id;
                for (var j=0; j<cantidades.length;j++){
                if (("mas"+j) ==i){
                if (cantidades[j] < cantidadIni[j]){

                maxCant[j]--; //Según cliques el botón se descuenta una unidad del array de cantidad
                //Comprobación de que no se llegue al stock mínimo
                console.log(cantidades[j]);
                if(minCant[j] == maxCant[j]){ 
                                	if (precios[j] > 100.00){
                                		alert("Límite de stock alcanzado. En caso de pago final se enviará un correo al proveedor para soliciatar nuevas unidades.");
                                		maxCant[j] +=5;
                                	} else{
                                		alert("Límite de stock alcanzado. En caso de pago final se enviará un correo al proveedor para soliciatar nuevas unidades.");
                                		maxCant[j] +=15;
                                }
                                }
                document.getElementById("inp"+j).value++;
                cantidades[j]++;
                //Se aumentá la cantidad del artículo comprado en el array
                for (var p=0; p<TodasLineas.length;p++){
                	var art = TodasLineas[p].codArticulo;
                	if (("mas" + (art-1)) == i)
                	TodasLineas[p].cantidad++;
                }

                var num = precios[j]*(cantidades[j]);
                
                //Se calcula la suma con los artículos aumentados evitando que se duplique
                 for (var m=0; m<precioTotal.length;m++){
                	if ("mas"+m == i){
                		precioTotal[m] = num;
                		suma = (parseFloat(precios[m]) + parseFloat(suma));
                		final.innerHTML = "Total: " + parseFloat(suma) + "€";
                	}
                }
                console.log(cantidades);
                }else{
                	alert("No tenemos esta cantidad");}
                }
                }

             
                
                console.log(precioTotal);
	            }
                
                //Función para disminuir la cantidad
	            function menoscantidad(x){
                var i = x.id;
                for (var j=0; j<cantidades.length;j++){
                if (("menos"+j) == i){
                	//Si la cantidad es mayor o igual a 1 puede seguir disminuyendo
                if (cantidades[j] >=1){
                maxCant[j]++; //Se aumenta la cantidad
                document.getElementById("inp"+j).value--;
                cantidades[j]--;
                 for (var p=0; p<TodasLineas.length;p++){
                	var art = TodasLineas[p].codArticulo;
                	if (("menos" + (art-1)) == i)
                	TodasLineas[p].cantidad--;
                }
                suma = parseFloat(suma) -(parseFloat(precios[j]));
                var num = precios[j]*(cantidades[j]);

                for (var m=0; m<precioTotal.length;m++){
                	if ("menos"+m == i){
                		precioTotal[m] = num;
                		final.innerHTML = "Total: " + suma + "€";
                	}
                }
                //Si llega a 0 se borra la línea
                } else {
                var mirow = document.getElementById("trid"+j);
                mirow.parentElement.removeChild(mirow); 
                document.getElementById("product"+j).style.display = "block";

                }
                console.log(cantidades);
                }
                }

	            }
                 
                //Función para introducir tu NIF y comprobar si estás registrado o no
	            function comptuNIF(){
	            devol.style.display = "none";
	            tablaalmacen.style.display = "none";
	            tablafacturas.style.display = "none";

                console.log(TodasLineas);
                tunif.innerHTML = "Introduce tu NIF: <br><input type='text' id='valuenif' onblur='validardniarriba()'>";
                tunif.innerHTML += " <button type='button' onclick='validardniarriba()' class='btn btn-primary'>Enviar</button> <span style='color:red;' id='errordniarriba'></span>";
	            }
                 
                //Validación del NIF. Formato 8 números y una letra. No puede estar vacío.
	            function validardniarriba(){
	            	var dniintroducidoarriba = valuenif.value;
	            	if (!(/^[0-9XYZ][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/i.test(dniintroducidoarriba))){
	              errordniarriba.innerHTML = "*Error. Introduzca un NIF correcto.";
			       document.getElementById("valuenif").focus();
			       }
			       else {
			       	if(suma == 0){
                   errordniarriba.innerHTML = "*Error. No tiene ningún producto en el carrito.";
			       } else{
			       	errordniarriba.innerHTML = "";
			       	comprobarNIF(); //Si todo es correcto va a la siguiente función.
			       }
			       }

			        
	            }


                //Validación del NIF del siguiente input
	            function validarNIF(){
	            	var dniintroducido = reg_NIF.value;
	            	var otroNIF = valuenif.value;
	            	if (!(/^[0-9XYZ][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/i.test(dniintroducido))){
	              errordni.innerHTML = "*Error. Introduzca un NIF correcto.";
			       document.getElementById("reg_NIF").focus();
			       } 
			       
			       else {
			       	errordni.innerHTML = "";
			       }

			       if (otroNIF != dniintroducido){
			       	errordni.innerHTML = "*Error. No puede modificar el dni.";
			       	document.getElementById("reg_NIF").focus();
			       }
	            	}

	            
                //Validación del nombre y apellidos. No se pueden introducir números.
	            function validarnombre(){
                   var elnombre = reg_nombre.value;
                   if (!(/^[a-zA-ZñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ ]+$/.test(elnombre))){
			       errornombre.innerHTML = "*Error. Introduzca solo caracteres alfabéticos.";
			       document.getElementById("reg_nombre").focus();
			       } else {
			       	errornombre.innerHTML = "";
			       }
	            }

	            function validarapellidos(){
                   var apell = reg_apellidos.value;
                   if (!(/^[a-zA-ZñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ ]+$/.test(apell))){
			       errorapell.innerHTML = "*Error. Introduzca solo caracteres alfabéticos.";
			       document.getElementById("reg_apellidos").focus();
			       } else {
			       	errorepell.innerHTML = "";
			       }
	            }
                
                //Validación de la dirección. No se puede dejar vacía. Si admite numeros.
	            function validardirec(){
                  if(reg_dir.value == ""){
                   errordir.innerHTML = "*Error. Introduzca su dirección.";
			       document.getElementById("reg_dir").focus();
                  } else {
                  errordir.innerHTML = "";

                  }
	            }
                
                //Validación de la población. No se puede dejar vacía. Si admite números.
	            function validarpoblacion(){
                  if(reg_poblacion.value === ""){
                   errorpob.innerHTML = "*Error. Introduzca su población.";
			       document.getElementById("reg_poblacion").focus();
                  } else {
                  errorpob.innerHTML = "";
                  }
	            }

                 //Validación del correo. Solo formato correo válido.
	             function validarcorreo(){
                  var elcorreo = reg_correo.value;
                   if (!(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(elcorreo))){
                   errorcor.innerHTML = "*Error. Dirección de correo errónea.";
			       document.getElementById("reg_correo").focus();
                  } else {
                  errorcor.innerHTML = "";
                  }
	            }
                
                //Función para comprobar si el NIF está en la BD o no. Si está se muestran todos los datos por si se quieren modificar. Si no, los datos para rellenar.
	            function comprobarNIF(){
	            devol.style.display = "none";
	            tablaalmacen.style.display = "none";
	            tablafacturas.style.display = "none";
                
	            var elNIF = document.getElementById("valuenif").value;

	            //Se rellena el array todas Lineas con el NIF y la suma necesarios para incluir en la factura.
	            for (var x=0; x<TodasLineas.length; x++){
	            	if (TodasLineas[x].dni == "dni"){
	            		TodasLineas[x].dni = elNIF;
	            	}
	            	if (TodasLineas[x].sumaTotal == "suma"){
	            		TodasLineas[x].sumaTotal = suma;
	            	}
	            }
	            console.log(TodasLineas);

	            console.log(TodasLineas);
                var objAjax = new XMLHttpRequest();
				objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1){  								         mensajenoregistrado.innerHTML = "*Tu NIF no está registrado. Completa los siguientes campos:";
								  datos.style.display = 'block';
								  document.getElementById("reg_NIF").value = elNIF;
								  } 
						else {
							var usuarios = new Array();
							usuarios = JSON.parse(this.responseText);
							for (var i=0; i<usuarios.length;i++){
								if(elNIF == usuarios[i].dniUsu){
								mensajenoregistrado.innerHTML = "*Estás registrado. Deseas modificar algún campo: ";
                                  datos.style.display = 'block';
								  document.getElementById("reg_NIF").value = elNIF;
								  document.getElementById("reg_nombre").value = usuarios[i].usuNombre;
                                  document.getElementById("reg_apellidos").value = usuarios[i].usuApellidos;
                                  document.getElementById("reg_dir").value = usuarios[i].usuDireccion;
                                  document.getElementById("reg_poblacion").value = usuarios[i].usuPoblacion;
                                  document.getElementById("reg_correo").value = usuarios[i].usuCorreo;
                                  
								}
							}
							}
				};
				//Se le pasa el valor del NIF para comprobar si existe en la base de datos
				objAjax.open("GET", "Menu/comprobacionNIF.php?elNIF="+elNIF, true);
				objAjax.send(null);
	            }
          
          //Validación de que al clicar no exista ningún campo vacío.
          function misvalidaciones(){
          	if (reg_poblacion.value != "" && reg_correo.value!= "" && reg_nombre.value != "" && reg_apellidos.value != "" && reg_NIF.value != "" && reg_dir.value != ""){
               registroUsus(); //Si todo es correcto pasa a la siguiente función.
          	} else{
          		alert("Completa todos los campos");
          	}
          }

          //Función para enviar el formulario mediante ajax e introducir a los usuarios en la BD.
       function registroUsus() {
       	        devol.style.display = "none";
       	        tablaalmacen.style.display = "none";
       	        tablafacturas.style.display = "none";
				var formulario = new FormData(document.getElementById("registroForm"));
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onload = function(oEvent) {
					if ((xmlhttp.status == 200) && (xmlhttp.readyState == 4)){
						alert(xmlhttp.responseText);
						pagobtn.style.display = "block"; //Si fueron introducidos correctamente se muestra este botón
					}
					else {
						alert("Error");
					}
				}
				xmlhttp.open("POST", "Menu/registroUsuarios.php", true);
				xmlhttp.send(formulario);
				console.log(formulario);
				for (var [key, value] of formulario.entries()) { 
  				console.log(key, value);

				}
			}
			console.log(TodasLineas);
        
        //Función para enviar el array TodasLineas con todos los datos de la compra a la BD
		function pagar(){
		devol.style.display = "none";
		tablaalmacen.style.display = "none";
		datos.style.display= "none";
	    mensajenoregistrado.innerHTML= "";
	    pagobtn.style.display= "none";
	    lineas.innerHTML = "";
	    titulolineas.innerHTML = "";
	    final.innerHTML = "";
	    tunif.innerHTML= "";
	    btncompra.style.display = "none";
	    tablafacturas.style.display = "none";
	    var miJSON = JSON.stringify(TodasLineas); //Transformo el array JS a JSON
	    TodasLineas.length = 0;
		var objAjax = new XMLHttpRequest();
		objAjax.onreadystatechange = function() {
			if (objAjax.readyState == 4 && objAjax.status == 200)
				alert(objAjax.responseText);
		};
		objAjax.open("POST", "Menu/insertarLineas.php", true);
		objAjax.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
        objAjax.send(miJSON);
		console.log(miJSON);
	}

    //Al cliclar se muestra el div con los inputs para hacer la devolución
	function misdevol(){
		devol.style.display = "block";
		listaproductos.style.display = "none";
		datos.style.display = "none";
		mensajenoregistrado.style.display = "none";
		pagobtn.style.display= "none";
		tablaalmacen.style.display = "none";
		tablafacturas.style.display = "none";
		catalbody.style.display = "none";
		tunif.style.display = "none";
		tituloarts.style.display="none";
		titproductos.style.display= "none";
	}

	//Objeto para construir el array de las devoluciones

	class LaDevolucion{
		constructor(idproducto,cantidadDevuelta,factnum,pvp){
	            this.idproducto = idproducto;
	            this.cantidadDevuelta = cantidadDevuelta;
	            this.factnum = factnum;
	            this.pvp = pvp;
		}
	}

	//Arrays necesarios para las devoluciones
    var devols = new Array();
    var contador = new Array();
    var totalcant = new Array();
    var elnumfact;
    
    //Validación del NIF.Formato 8 numeros y 1 letra.
    function nifdevolvalidacion(){
                   var devolnif = nifdevolucion.value;
                   if (!(/^[0-9XYZ][0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKE]$/i.test(devolnif))){
	               errornifdevol.innerHTML = "*Error. Introduzca un NIF correcto.";
			       document.getElementById("nifdevolucion").focus();
			       } else {
			       	errornifdevol.innerHTML = "";
			       }
                   
	            }

    //Validación número de fáctura. Solo números.
	function numdevolvalidacion(){
                  var devolnum = numfacturadevol.value;
                   if (!(/^([0-9])*$/.test(devolnum))){
	               errornumdevol.innerHTML = "*Error. Introduzca un número de factura correcto.";
			       document.getElementById("numfacturadevol").focus();
			       } else {
			       	errornumdevol.innerHTML = "";
			       }
	}
    
    //Función para controlar que si están vacios no pueda continuar.
	function devolvalidaciones(){
          	if (numfacturadevol.value != "" && nifdevolucion.value!= ""){
               muestramislineas(); //Si es correcto se pasa a la siguiente función.
          	} else{
          		alert("Completa todos los campos");
          	}
          }
    
    //Función en la que a través de ajax, pasándole un array con tu compra según tu NIF y número de fáctura
	function muestramislineas(){
		tablafacturas.style.display = "none";
        tablaalmacen.style.display = "none";
		var nifdevol = nifdevolucion.value;
		elnumfact= numfacturadevol.value;
		var objAjax = new XMLHttpRequest();
		tabladevoluciones.innerHTML = " ";
		tabladevoluciones.innerHTML = "<thead><tr><th></th><th>Nombre</th><th>Código</th><th>Cantidad pedida</th></tr></thead>";
				objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1) 
							alert(objAjax.responseText);
						else {
							tucomprapasada = JSON.parse(this.responseText);
							var fechahoy = new Date();
							var hoyformato = fechahoy.getFullYear() + "-" + (fechahoy.getMonth() +1) + "-" + fechahoy.getDate();
							var date1 = new Date(hoyformato);
                            for (var i=0; i<tucomprapasada.length;i++){
                            	//Comprobación de las fechas
                            	var date2 = new Date(tucomprapasada[i].fecha);
                                var resta= date1.getTime()-date2.getTime();
                            	var dias = Math.round(resta/ (1000*60*60*24));
                            	if (nifdevol === tucomprapasada[i].dni){
                            		//Si es menor o igual a 16 puede acceder. Si no, ya no puede hacer la devolución
                            		if(dias<=16){
                            			tabladevoluciones.style.display = "block";
                            			message.innerHTML = " ";
                            			                     			
                            		    contador.push(0);
                            		    totalcant.push(tucomprapasada[i].cantidad);
                            		    
                            			
 
                            		tabladevoluciones.innerHTML += "<tr><td><img style='width:60px;height:60px;' src='"+ tucomprapasada[i].imagenes +"'></td><td>"+ tucomprapasada[i].nombre+ "</td><td>" + tucomprapasada[i].codArticulo +"</td><td>"+ tucomprapasada[i].cantidad+ "</td><td><button type='button' id='dev"+i+"' class='btn btn-light' onclick='hasdevolution(this)'>Devolver</button></td></tr>";
                            	    }else {
                            	    	tabladevoluciones.style.display = "none";
                            	    	message.innerHTML = "Ya han transcurrido más de 16 días desde su compra. No puede realizar ninguna devolución.";
                            	    	
                            	    }
                            	}else {
                                  message.innerHTML = "Lo sentimos. Su NIF no corresponde a esta factura.";
                            	}
                            	
                            }
                             }
							
				};
				//Le paso el número de factura para comprobar si existe en la base de datos
				objAjax.open("GET", "Menu/infolineas.php?elnumfact="+elnumfact, true);
				objAjax.send(null);
	            
	}
	      

	     //Función para ir mostrando las devoluciones según se vaya clicando  	
         function hasdevolution(b){
         console.log(totalcant);
        var id = b.id;
        tablaalmacen.style.display = "none";
        tablafacturas.style.display = "none";
		var objAjax = new XMLHttpRequest();
				objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1) 
							alert(objAjax.responseText);
						else {
							var tucompra = new Array();
							tucompra = JSON.parse(this.responseText);
                            for (var i=0; i<tucompra.length;i++){
                            	if(("dev"+i) == id){
                            		//Se pregunta cuantos se quieren devolver con como máximo la cantidad disponible que habías comprado
                            		var quant = parseInt(prompt("Puedes devolver "+tucompra[i].cantidad+" como máximo. Cuantas unidades deseas devolver de este producto: "));
                            		//En el contador cambio la cantidad introducida por la que había anteriormente
                            		if (quant <= parseInt(totalcant[i]) && quant>0){
                                        totalcant[i]-= quant;
                            			contador[i] = quant;
                            			console.log(contador);
                            	    //Se muestran los productos

                            		titulodevol.style.display = "block";
                            		listaadevolver.style.display = "block";
                            		var lista = "<tr><td><img style='width:60px;height:60px;' src='"+ tucompra[i].imagenes +"'></td><td>"+ tucompra[i].nombre+ "</td><td>"+contador[i]+"</td></tr>";
                            		btnform.style.display = "block";
                                    
                                    //Se van incluyendo los productos devueltos en el array
                            		var totaldevoluciones = new LaDevolucion(tucompra[i].codArticulo,contador[i],tucompra[i].numFactura, tucompra[i].PVP);
                            		devols.push(totaldevoluciones);
                            		console.log(devols);

                            		
                             } else{alert("No tienes cantidad suficiente para devolver.");
                                         };
                            	    }
                            	    }
                                    if(lista!=null){
                            	    listaadevolver.innerHTML += lista;
                            	    }
                                     
							}
				};
				objAjax.open("GET", "Menu/infolineas.php?elnumfact="+elnumfact, true);
				objAjax.send(null);
           }
        
        //Función para pasar el array de las devoluciones a la base de datos
         function enviardatos(){
         tablafacturas.style.display = "none";
        titulodevol.style.display = "none";
        listaadevolver.innerHTML = " ";
        btnform.style.display = "none";
        tabladevoluciones.style.display = "none";
        totalcant.length = 0;
        contador.length = 0;
	    var miJSON = JSON.stringify(devols); //Se transforma en JSON
	    devols.length = 0;
		var objAjax = new XMLHttpRequest();
		objAjax.onreadystatechange = function() {
			if (objAjax.readyState == 4 && objAjax.status == 200)
				alert(objAjax.responseText);
		};
		objAjax.open("POST", "Menu/actualizardevoluciones.php", true);
		objAjax.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
        objAjax.send(miJSON);
        console.log(miJSON);

	}
    
    //Función para mostrar los productos disponibles en el almacén
	function almacen(){
    catalbody.style.display = "none";
    devol.style.display = "none";
	listaproductos.style.display = "none";
    tablaalmacen.style.display = "block"
    datos.style.display = "none";
	mensajenoregistrado.style.display = "none";
	pagobtn.style.display= "none";
	tablafacturas.style.display = "none";
	tunif.style.display = "none";
	titproductos.style.display= "none";


	var objAjax = new XMLHttpRequest();
			objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1)
							alert(objAjax.responseText);
						
						else { 
							tituloarts.style.display = "block";
							var milistado = "<thead><tr><th scope='col'>#</th><th scope='col'>Código Producto</th><th scope='col'>Nombre</th><th scope='col'>Cantidad</th><th scope='col'>PVP</th></tr></thead><tbody>";
							var tienda = new Array();
							tienda = JSON.parse(this.responseText);
							for (i=0; i< tienda.length; i++){
								milistado += "<tr><th scope='row'>"+(i+1)+"</th><td>"+tienda[i].codArticulo+"</td><td>"+tienda[i].nombre+"</td><td>"+tienda[i].cantidad+"</td><td>"+tienda[i].pvp+"€</td></tr>";

							}

							milistado += "</tbody>";
							tablaalmacen.innerHTML = milistado;
						}
				};
				objAjax.open("GET", "Menu/listado.php", true);
				objAjax.send(null);
	}

    //Función para mostrar las facturas con una paginación de 10 por página
	function todasFacturas(_page){
    devol.style.display = "none";
	listaproductos.style.display = "none";
    tablaalmacen.style.display = "block"
    datos.style.display = "none";
	mensajenoregistrado.style.display = "none";
	pagobtn.style.display= "none";
	tablaalmacen.style.display = "none";
	tablafacturas.style.display = "block";
	catalbody.style.display = "none";
	tunif.style.display = "none";
	tituloarts.style.display = "none";
   
	var objAjax = new XMLHttpRequest();
			objAjax.onreadystatechange = function() {
					if (objAjax.readyState == 4 && objAjax.status == 200)
						if (objAjax.responseText.indexOf("ERROR:")>-1)
							alert(objAjax.responseText);
						else{
							tablafacturas.innerHTML = objAjax.responseText;
						}

				
				};
				objAjax.open("GET", "Menu/listafacturas.php?page=" + _page, true);
				objAjax.send(null);
	}

    

                   
</script>
</body>
</html>
