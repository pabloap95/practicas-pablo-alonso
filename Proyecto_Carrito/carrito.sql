-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-04-2021 a las 20:45:02
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carrito`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `codArticulo` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `pvp` decimal(8,2) NOT NULL,
  `cantidad` int(4) NOT NULL,
  `cantidadMin` int(2) NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `imagenes` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`codArticulo`, `nombre`, `pvp`, `cantidad`, `cantidadMin`, `correo`, `imagenes`) VALUES
(1, 'PC', '900.00', 14, 5, 'ordenadoresnuestros@gmail.com', 'Imagenes/pc.jpg'),
(2, 'Móvil', '600.00', 9, 5, 'samsung@gmail.com', 'Imagenes/movil.jpg'),
(3, 'Robot de cocina', '1300.00', 14, 5, 'misrobots\"gmai.com', 'Imagenes/robotcocina.jpg'),
(4, 'Cámara GoPro', '389.99', 9, 5, 'elcorteingles@gmail.com', 'Imagenes/camaragopro.jpg'),
(5, 'iPad', '379.20', 10, 5, 'apple@gmail.com', 'Imagenes/ipad.jpg'),
(6, 'Ratón', '25.50', 14, 10, 'ratonesatudisposicion@gmail.co', 'Imagenes/raton.jpg'),
(7, 'Selfie Stick', '55.00', 12, 10, 'palosdeselfie@gmail.com', 'Imagenes/selfiestick.jpg'),
(8, 'Headphones', '87.25', 17, 10, 'appleheadphones@gmail.com', 'Imagenes/headphones.jpg'),
(9, 'Teclado', '66.00', 25, 10, 'appleteclados@gmail.com', 'Imagenes/teclado.jpg'),
(10, 'Auriculares inalámbricos', '38.99', 23, 10, 'samsumgauriculares@gmail.com', 'Imagenes/auricularesinam.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `numFactura` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `dni` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `eurosBI` decimal(8,2) NOT NULL,
  `iva` decimal(3,2) NOT NULL,
  `eurosIVA` decimal(8,2) NOT NULL,
  `eurostotal` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`numFactura`, `fecha`, `dni`, `eurosBI`, `iva`, `eurosIVA`, `eurostotal`) VALUES
(7, '2021-03-29', '11111111A', '1394.66', '0.21', '292.88', '1687.54'),
(8, '2021-03-29', '11111111A', '753.25', '0.21', '158.18', '911.43'),
(9, '2021-03-29', '22222222B', '643.19', '0.21', '143.26', '825.45'),
(11, '2021-03-29', '22222222B', '1387.25', '0.21', '102.32', '589.57'),
(12, '2021-03-29', '33333333C', '989.99', '0.21', '207.90', '1197.89'),
(13, '2021-03-29', '11111111A', '415.49', '0.21', '87.25', '502.74'),
(15, '2021-03-29', '22222222B', '1355.00', '0.21', '284.55', '1639.55'),
(16, '2021-03-29', '22222222B', '1355.00', '0.21', '284.55', '1639.55'),
(17, '2021-03-29', '22222222B', '1355.00', '0.21', '284.55', '1639.55'),
(18, '2021-03-29', '22222222B', '2200.00', '0.21', '462.00', '2662.00'),
(21, '2021-03-31', '22222222B', '1355.00', '0.21', '284.55', '1639.55'),
(22, '2021-04-01', '11111111A', '900.00', '0.21', '189.00', '1089.00'),
(24, '2021-04-01', '22222222B', '379.20', '0.21', '79.63', '458.83'),
(25, '2021-04-01', '55555555E', '455.99', '0.21', '95.76', '551.75'),
(26, '2021-04-01', '66666666F', '900.00', '0.21', '189.00', '1089.00'),
(27, '2021-04-02', '22222222B', '900.00', '0.21', '189.00', '1089.00'),
(28, '2021-04-02', '11111111A', '1616.43', '0.21', '339.45', '1955.88'),
(29, '2021-04-02', '22222222B', '1689.99', '0.21', '354.90', '2044.89'),
(30, '2021-04-02', '22222222B', '418.19', '0.21', '87.82', '506.01'),
(31, '2021-04-02', '22222222B', '966.00', '0.21', '202.86', '1168.86'),
(32, '2021-04-02', '22222222B', '1378.99', '0.21', '289.59', '1668.58'),
(33, '2021-04-02', '22222222B', '989.99', '0.21', '207.90', '1197.89'),
(34, '2021-04-02', '11111111A', '434.20', '0.21', '91.18', '525.38'),
(35, '2021-04-02', '66666666F', '389.99', '0.21', '81.90', '471.89'),
(36, '2021-04-03', '22222222B', '415.49', '0.21', '87.25', '502.74'),
(38, '2021-04-03', '44444444D', '1173.99', '0.21', '246.54', '1420.53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iva`
--

CREATE TABLE `iva` (
  `iva` decimal(3,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `iva`
--

INSERT INTO `iva` (`iva`) VALUES
('0.21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas`
--

CREATE TABLE `lineas` (
  `numFactura` int(10) NOT NULL,
  `codArticulo` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` int(5) NOT NULL,
  `PVP` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `lineas`
--

INSERT INTO `lineas` (`numFactura`, `codArticulo`, `fecha`, `cantidad`, `PVP`) VALUES
(7, 5, '2021-03-29', 1, 379),
(7, 9, '2021-03-29', 1, 66),
(7, 10, '2021-03-29', 1, 39),
(8, 2, '2021-03-29', 1, 600),
(8, 9, '2021-03-29', 1, 66),
(9, 5, '2021-03-29', 1, 379),
(9, 9, '2021-03-29', 4, 66),
(11, 3, '2021-03-29', 1, 1300),
(11, 8, '2021-03-29', 1, 87),
(12, 2, '2021-03-29', 1, 600),
(12, 4, '2021-03-29', 1, 390),
(13, 4, '2021-03-29', 1, 390),
(13, 6, '2021-03-29', 1, 26),
(15, 3, '2021-03-29', 1, 1300),
(15, 7, '2021-03-29', 1, 55),
(16, 2, '2021-03-29', 4, 600),
(16, 3, '2021-03-29', 1, 1300),
(16, 7, '2021-03-29', 1, 55),
(17, 2, '2021-03-29', 5, 600),
(17, 3, '2021-03-29', 1, 1300),
(17, 7, '2021-03-29', 1, 55),
(18, 1, '2021-03-29', 1, 900),
(18, 3, '2021-03-29', 1, 1300),
(21, 3, '2021-03-31', 1, 1300),
(21, 7, '2021-03-31', 1, 55),
(22, 1, '2021-04-01', 1, 900),
(24, 5, '2021-04-01', 1, 379),
(25, 4, '2021-04-01', 1, 390),
(25, 9, '2021-04-01', 1, 66),
(26, 1, '2021-04-01', 1, 900),
(27, 1, '2021-04-02', 1, 900),
(28, 2, '2021-04-02', 1, 600),
(28, 4, '2021-04-02', 1, 390),
(28, 5, '2021-04-02', 1, 379),
(28, 7, '2021-04-02', 1, 55),
(28, 8, '2021-04-02', 1, 87),
(28, 9, '2021-04-02', 1, 66),
(28, 10, '2021-04-02', 2, 39),
(29, 3, '2021-04-02', 1, 1300),
(29, 4, '2021-04-02', 1, 390),
(30, 5, '2021-04-02', 1, 379),
(30, 10, '2021-04-02', 1, 39),
(31, 1, '2021-04-02', 1, 900),
(31, 9, '2021-04-02', 1, 66),
(32, 2, '2021-04-02', 1, 600),
(32, 4, '2021-04-02', 2, 390),
(33, 2, '2021-04-02', 1, 600),
(33, 4, '2021-04-02', 1, 390),
(34, 5, '2021-04-02', 1, 379),
(34, 7, '2021-04-02', 1, 55),
(35, 4, '2021-04-02', 1, 390),
(36, 4, '2021-04-03', 1, 390),
(36, 6, '2021-04-03', 1, 26),
(38, 1, '2021-04-03', 1, 900),
(38, 7, '2021-04-03', 1, 55),
(38, 9, '2021-04-03', 1, 66),
(38, 10, '2021-04-03', 4, 39);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `dniUsu` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `usuNombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuApellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuDireccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `usuPoblacion` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `usuCorreo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dniUsu`, `usuNombre`, `usuApellidos`, `usuDireccion`, `usuPoblacion`, `usuCorreo`) VALUES
('11111111A', 'Maria Luisa', 'Presumido Soldado', 'Calle Hernán Cortés', 'raices nuevo castrillon', 'mariaLu@gmail.com'),
('22222222B', 'Pablo', 'Presumido', 'Calle Hernán Cortés N°9, 2°C', 'raices nuevo castrillon', 'pabloap@gmail.com'),
('33333333C', 'Clara', 'Lago Perez', 'C/El pez', 'Sevilla', 'claralago@gmail.com'),
('44444444D', 'Juan', 'Campos Rodriguez', 'C/Val Alere', 'Santander', 'juan@gmail.com'),
('55555555E', 'Alberto', 'A E', 'sdfs', 'dgsg', 'alberto@gmail.com'),
('66666666F', 'Lola', 'Perez Tomas', 'c/La alegria', 'Oviedo', 'lola@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`codArticulo`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`numFactura`),
  ADD KEY `facturas_ibfk_1` (`dni`);

--
-- Indices de la tabla `iva`
--
ALTER TABLE `iva`
  ADD PRIMARY KEY (`iva`);

--
-- Indices de la tabla `lineas`
--
ALTER TABLE `lineas`
  ADD PRIMARY KEY (`numFactura`,`codArticulo`),
  ADD KEY `lineas` (`codArticulo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`dniUsu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `codArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `numFactura` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `facturas_ibfk_1` FOREIGN KEY (`dni`) REFERENCES `usuarios` (`dniUsu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lineas`
--
ALTER TABLE `lineas`
  ADD CONSTRAINT `facturas_ibfk_2` FOREIGN KEY (`numFactura`) REFERENCES `facturas` (`numFactura`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lineas` FOREIGN KEY (`codArticulo`) REFERENCES `articulos` (`codArticulo`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
