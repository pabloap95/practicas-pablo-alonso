<?php
header('Content-Type:application/json; charset=UTF-8');

$jsonarray =  file_get_contents('php://input');
$nuevojson = json_decode($jsonarray,true);

require('../Utils/conexion.php');

$iva = "SELECT iva FROM iva";
if($result = mysqli_query($con, $iva)){
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            $miIVA = $row['iva'];
        }
    } else{
        echo "No records matching your query were found.";
    }
   }

if(is_array($nuevojson) || is_object($nuevojson)){
foreach ($nuevojson as $data) { 
$idproducto = $data['idproducto'];
$cantidadDevuelta = $data['cantidadDevuelta'];
$factnum = $data['factnum'];
$PVP = $data['pvp'];

$nuevafactura = mysqli_query($con,"
  UPDATE facturas 
     SET eurosBI = eurosBI - ('$PVP'*'$cantidadDevuelta')
   WHERE numFactura = '$factnum';");

$acteurosIVA = mysqli_query($con,"
  UPDATE facturas 
     SET eurosIVA = eurosBI*'$miIVA'
   WHERE numFactura = '$factnum';");

$actTotal = mysqli_query($con,"
  UPDATE facturas 
     SET eurostotal = eurosBI + eurosIVA
   WHERE numFactura = '$factnum';");

$devoluciones = mysqli_query($con,"
  UPDATE articulos 
     SET cantidad = cantidad + '$cantidadDevuelta'
   WHERE codArticulo = '$idproducto';");
   
$updatelineas = mysqli_query($con,"
  UPDATE lineas
     SET cantidad = cantidad - '$cantidadDevuelta'
   WHERE numFactura = '$factnum'
   AND codArticulo = '$idproducto';");

$eliminar = mysqli_query($con,"
  DELETE FROM lineas 
   WHERE cantidad = 0 
   AND codArticulo = '$idproducto';");

$nuevafactura = mysqli_query($con,"
  DELETE FROM facturas 
   WHERE eurosBI = 0;");

}
 }


echo "Su devolución se ha realizado correctamente. Disculpamos las molestias.";


$con->close();
?> 
