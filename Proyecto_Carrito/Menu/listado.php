<?php
	header("Content-Type: text/html;charset=utf-8");

	require('../Utils/conexion.php');

	$catalogo = "SELECT * FROM articulos";

	if (!$resultado = $con->query($catalogo)) {
		echo "ERROR: La conexion falla";
	} else if ($resultado->num_rows === 0) {
		echo "ERROR: No hay resultados";
	} else {
		$rawdata = array();
		$i=0;
		while($row = mysqli_fetch_array($resultado)) {
			$rawdata[$i] = $row;
			$i++;
		}
		$listado = json_encode($rawdata);
		exit($listado);
	}
?>